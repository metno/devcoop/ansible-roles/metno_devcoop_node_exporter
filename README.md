# Description

Ansible role to install [node_exporter](https://github.com/prometheus/node_exporter/) on a host.
